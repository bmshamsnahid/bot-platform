const { VariableInfo } = require('../../model/counter/variableInfo');
const _ = require('lodash');

const getBotVariableInfos = async (botId) => {
    const variableInfos = await VariableInfo.find({ botId: botId });
    return variableInfos;
};

const initiateVariableInfo = async (variableInfoId, fbUserId) => {
    let variableInfo = await VariableInfo.findById(variableInfoId);
    variableInfo.value = variableInfo.initialValue;
    variableInfo.fbUserId = fbUserId;
    await variableInfo.save();
    return true;
};

const initiateBotVariableInfos = async (botId, fbUserId) => {
    const variableInfos = await VariableInfo.find({ botId: botId, fbUserId: 'model' });
    const userVariablesInfos = await VariableInfo.find({ botId: botId, fbUserId: fbUserId });

    if (variableInfos.length == userVariablesInfos.length) {
        // already variableInfos are declared, need to update them
        for (let index=0; index<userVariablesInfos.length; index++) {
            let currentVariableInfo = userVariablesInfos[index];
            currentVariableInfo.value = currentVariableInfo.initialValue;
            currentVariableInfo.fbUserId = fbUserId;
            await currentVariableInfo.save();
        }
    } else {
        // variableInfos for the current user in this bot is not declared yet.
        for (let index=0; index<variableInfos.length; index++) {
            let currentVariableInfo = variableInfos[index];
            const newVariableInfo = new VariableInfo(_.pick(currentVariableInfo, ['name', 'description', 'variableType', 'initialValue', 'value', 'botId', 'fbUserId']));
            newVariableInfo.value = newVariableInfo.initialValue;
            newVariableInfo.fbUserId = fbUserId;
            await newVariableInfo.save();
        }
    }

    return;
}

module.exports = {
    getBotVariableInfos,
    initiateVariableInfo,
    initiateBotVariableInfos
};
