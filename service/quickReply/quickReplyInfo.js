const { QuickReplyInfo, validateQuickReplyInfo } = require('../../model/quickReply/quickReplyInfo');
const _ = require('lodash');

const createQuickReplyInfo = async (quickReply) => {
    const { error } = validateQuickReplyInfo(quickReply);
    if (error) return { error : error };

    const quickReplyInfo = new QuickReplyInfo(quickReply);
    const result = await quickReplyInfo.save();
    
    return {
        error: null,
        data: result
    };
};

const getQuickReplyInfos = async () => {
    const quickReplyInfos = await QuickReplyInfo.find();
    if (!quickReplyInfos) return { error: 'Internal server error' }
    return {
        error: null,
        data: quickReplyInfos
    }
};

const getQuickReplyInfo = async (id) => {
    const quickReplyInfo = await QuickReplyInfo.findById(id);
    if (!quickReplyInfo) return { error: 'Invalid quick Reply Info Id' }
    return {
        error: null,
        data: quickReplyInfo
    }
};

const updateQuickReplyInfo = async (id, newData) => {
    let quickReplyInfo = await QuickReplyInfo.findById(id);
    if (!quickReplyInfo) return { error: 'Invalid quick reply info id' };

    quickReplyInfo.content_type = newData.content_type || quickReplyInfo.content_type;
    quickReplyInfo.title = newData.title || quickReplyInfo.title;
    quickReplyInfo.payload = newData.payload || quickReplyInfo.payload ;

    const { error } = validateQuickReplyInfo(quickReplyInfo);
    if (error) return { error : error };

    const result = await quickReplyInfo.save();

    return {
        error: null,
        data: result
    };
};

const deleteQuickReplyInfo = async () => {
    let quickReplyInfo = await QuickReplyInfo.findById(id);
    if (!quickReplyInfo) return { error: 'Invalid quickReply id' };
    return {
        error: null,
        data: quickReplyInfo
    }
};

const createMultipleQuickReplies = async (quickReplies) => {
    const validatedQuickReplies = [];
    const quickRepliesId = [];
    
    // validate all the quick replies
    for (let index=0; index<quickReplies.length; index++) {
        const { error } = validateQuickReplyInfo(quickReplies[index]);
        if (error) {
            return {
                error: error,
                quickRepliesId: null
            }
        } else {
            const model = new QuickReplyInfo(quickReplies[index]);
            validatedQuickReplies.push(model);
        }
    }

    // saving the quick replies
    for (let index=0; index<validatedQuickReplies.length; index++) {
        const currentValidatedQuickReply = validatedQuickReplies[index];
        await currentValidatedQuickReply.save();
        quickRepliesId.push(currentValidatedQuickReply._id);
    }

    return {
        error: null,
        quickRepliesId: quickRepliesId
    }
};

const updateQuickRepliesPreviousNodeAndId = async (quickReplyInfosId, previousNodeType, previousNodeId) => {
    const previosNodePaylaodInfo = '-' + previousNodeType + '-' + previousNodeId;

    for (let index=0; index<quickReplyInfosId.length; index++) {
        let quickReplyInfo = await QuickReplyInfo.findById(quickReplyInfosId[index]);
        if (!quickReplyInfo) return { error: 'Invalid Quick Reply Info Id', };

        quickReplyInfo.payload = previosNodePaylaodInfo;

        await quickReplyInfo.save();
    }

    return {
        error: null,
    }
};

module.exports = {
    createQuickReplyInfo,
    getQuickReplyInfos,
    getQuickReplyInfo,
    updateQuickReplyInfo,
    deleteQuickReplyInfo,
    createMultipleQuickReplies,
    updateQuickRepliesPreviousNodeAndId,
};