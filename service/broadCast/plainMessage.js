const textMessage = require('../../messageSend/text');

const broadCast = async (userId, pageId, messageText) => {
    await textMessage(userId, messageText, pageId);
};

module.exports = {
    broadCast,
};
