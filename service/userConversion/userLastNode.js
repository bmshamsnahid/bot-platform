const { UserLastNode, validateUserLastNode } = require('../../model/UserInteraction/userLastNode');
const moment = require('moment');
const chalk = require('chalk');
const { TextMessage } = require('../../model/message/text');

// already a last node found, then updae the `last node id` and `last node type`
// for a new user create a new last node
const handleUserLastNode = async (pageId, userId, messageId, messageType) => {
    // if the new node is the last plain message, then we wont create or update the last node
    if (messageType == 'plain') {
        const textMessage = await TextMessage.findById(messageId);
        if (textMessage.isLastMessage) {
            return;
        }
    }
    const existingLastNode = await UserLastNode.findOne({ pageId: pageId, userId: userId });
    if (existingLastNode) {
        existingLastNode.lastNodeId = messageId;
        existingLastNode.lastNodeType = messageType;
        await existingLastNode.save();
        return;
    } else {
        const userLastNodeBody = {
            pageId: pageId,
            userId: userId,
            lastNodeType: messageType,
            lastNodeId: messageId,
            time: moment.now(),
            event: {},
            isEnable: true,
        };
        const { error } = validateUserLastNode(userLastNodeBody);
        if (error) {
            console.log(chalk.red(error.message));
            console.log(error);
        } else {
            const userLastNode = new UserLastNode(userLastNodeBody);
            await userLastNode.save();
            return;
        }
    }
};

const deleteLastNode = async (userId, pageId) => {
    const result = await UserLastNode.findOneAndDelete({ userId: userId, pageId: pageId });
    return result;
};

module.exports = {
    handleUserLastNode,
    deleteLastNode,
};
