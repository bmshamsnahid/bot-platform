const chalk = require('chalk');
const { UserQa } = require('../../model/UserInteraction/userQa');
const { UserLog } = require('../../model/UserInteraction/userLog');
const userLastNodeService = require('../../service/userConversion/userLastNode');

const createUserLog = async (userId, pageId) => {
    const userQas = await UserQa.find({ userId: userId, pageId: pageId, isLogged: false });
    await UserQa.update({ 
        userId: userId,
        pageId: pageId,
        isLogged: false,
    }, {
        $set: {
            isLogged: true,
        }
    }, {
        multi: true,
    });
    let log = [];
    for (let index=0; index<userQas.length; index++) {
        const currentUserQa = userQas[index];
        const question = currentUserQa.question;
        const answer = currentUserQa.answer;
        const qaId = currentUserQa._id;
        const logSchema = {
            question: question,
            answer: answer,
            qaId: qaId,
        };
        log.push(logSchema);
    }
    const userLog = new UserLog({
        log: log,
        pageId: pageId,
        userId: userId,
    });
    await userLog.save();
    await userLastNodeService.deleteLastNode(userId, pageId);
    return; 
};

module.exports = {
    createUserLog,
};
