const chalk = require('chalk');

const { UserQa, validateUserQaSchema } = require('../../model/UserInteraction/userQa');
const { QuickReplyMessage } = require('../../model/message/quickReplyMessage');
const { TextMessage } = require('../../model/message/text');
const { UserLastNode } = require('../../model/UserInteraction/userLastNode');

const facebookProfileService = require('../facebookProfile/profile');

const createQucikReplyConversion = async (quickReply, event) => {
    const payloadArray = quickReply.payload.split('-');

    if (payloadArray.length != 4) return;
    
    const nextNodeType = payloadArray[0];
    const nextNodeId = payloadArray[1];
    const previousNodeType = payloadArray[2];
    const previousNodeId = payloadArray[3];
    
    const quickReplyMessage = await QuickReplyMessage.findById(previousNodeId);
    
    const senderId = event.sender.id; // user id
    const recipientId = event.recipient.id; // page id

    const facebookUser = await facebookProfileService.getUserProfileInformation(senderId, recipientId);

    const userQaBody = {
        previousNodeType: previousNodeType,
        previousNodeId: previousNodeId,
        event: event,
        facebookUser: facebookUser,
        question: quickReplyMessage.text,
        answer: event.message.text,
        nextNodeType: nextNodeType,
        nextNodeId: nextNodeId,
        userId: senderId,
        pageId: recipientId,
    };

    const { error } = validateUserQaSchema(userQaBody);
    if (error) { 
        console.log('Validattion error');
        console.log(chalk.red(error));
    } else {
        const userQa = new UserQa(userQaBody);
        await userQa.save();
    }
    return;
};

const createPlainMessageConversion = async (pageId, userId, event) => {
    // we save conversion, only if the previous message is plain text message
    const userLastNode = await UserLastNode.findOne({ pageId: pageId, userId: userId });
    if (userLastNode != null && userLastNode.lastNodeType == 'plain') {
        const plainMessageId = userLastNode.lastNodeId;
        const textMessage = await TextMessage.findById(plainMessageId);
        const question = textMessage.text;
        const answer = event.message.text;
        const previousNodeType = 'plain';
        const previousNodeId = plainMessageId;

        const senderId = event.sender.id; // user id
        const recipientId = event.recipient.id; // page id

        const facebookUser = await facebookProfileService.getUserProfileInformation(senderId, recipientId);

        const userQaBody = {
            previousNodeType: previousNodeType,
            previousNodeId: previousNodeId,
            event: event,
            facebookUser: facebookUser,
            question: question,
            answer: answer,
            userId: senderId,
            pageId: recipientId,
        };

        const { error } = validateUserQaSchema(userQaBody);
        if (error) { 
            console.log('Validattion error');
            console.log(chalk.red(error));
        } else {
            const userQa = new UserQa(userQaBody);
            await userQa.save();
            return;
        }
    }
    return;
};

module.exports = {
    createQucikReplyConversion,
    createPlainMessageConversion,
};
