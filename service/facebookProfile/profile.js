const request = require('../../helper/requestPromise');
const { BotInfo } = require('../../model/bot/botInfo');
const axios = require('axios');

var getUserProfileInformation = async (senderId, pageId) => {
    const botInfo = await BotInfo.findOne({ pageId: pageId });
    
    const { data } = await axios.get('https://graph.facebook.com/v3.2/' + senderId, {
        params: {
            access_token: botInfo.pageAccessToken
        }
      });
    return data;
};

module.exports = {
    getUserProfileInformation,
};
