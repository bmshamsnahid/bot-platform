const { ElementInfo } = require('../../model/element/elementInfo');
const buttonInfoService = require('../../service/button/buttonInfo');

const getMultipleElementInfos = async (elementIds) => {
    let elementInfos = [];
    
    for (let index=0; index<elementIds.length; index++) {
        const id = elementIds[index];
        const elementInfo = await ElementInfo.findById(id);
        
        if (elementInfo.buttonsId.length) {
            const { error, data } = await buttonInfoService.getMultpleButtonInfos(elementInfo.buttonsId);
            if (error) return { error: error, data: null };
            elementInfo.buttonInfos = data;
        }
        await elementInfos.push(elementInfo);
    }

    return {
        error: null,
        data: elementInfos
    };
};

const modifyElementForTemplate = async (elementInfos) => {
    let modElements = [];
    for (let index=0; index<elementInfos.length; index++) {
        let modElement = {};
        const currentElement = elementInfos[index];
        
        modElement.title = currentElement.title;
        modElement.image_url = currentElement.imageUrl;
        modElement.subtitle = currentElement.subTitle;
        modElement.default_action = {
            type: currentElement.defaultActionType,
            url: currentElement.defaultActionUrl,
            webview_height_ratio: currentElement.defauktActionWebViewHeightRatio
        };
        modElement.buttons = await buttonInfoService.modifyButtonForTemplate(currentElement.buttonInfos);
        modElements.push(modElement);
    }
    return modElements;
};

module.exports = {
    getMultipleElementInfos,
    modifyElementForTemplate
};
