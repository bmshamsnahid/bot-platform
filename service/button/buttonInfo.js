const { ButtonInfo } = require('../../model/button/buttonInfo');

const getMultpleButtonInfos = async (buttonInfoIds) => {
    let buttonInfos = [];

    for (let index=0; index<buttonInfoIds.length; index++) {
        const id = buttonInfoIds[index];
        const buttonInfo = await ButtonInfo.findById(id);
        buttonInfos.push(buttonInfo);
    }

    return {
        error: null,
        data: buttonInfos
    }
};

const modifyButtonForTemplate = async (buttonInfos) => {
    let modButtons = [];
    for (let index=0; index<buttonInfos.length; index++) {
        const currentButton = buttonInfos[index];
        let modButton = {};
        modButton.type = currentButton.buttonType || '';
        modButton.url = currentButton.url || '';
        modButton.title = currentButton.buttonType || '';
        modButton.payload = currentButton.payload || '';
        modButtons.push(modButton);
    }
    return modButtons;
};

module.exports = {
    getMultpleButtonInfos,
    modifyButtonForTemplate
};
