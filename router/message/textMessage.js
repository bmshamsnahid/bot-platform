const express = require('express');
const router = express.Router();
const textMessageController = require('../../controller/message/text');

router.post('/', textMessageController.createMessage);
router.get('/', textMessageController.getMessages);
router.get('/:id', textMessageController.getMessage);
router.patch('/:id', textMessageController.updateMessage);
router.delete('/:id', textMessageController.deleteMessage);
router.patch('/firstMessageStatus/:id', textMessageController.updateFirstMessageStatus);
router.patch('/lastMessageStatus/:id', textMessageController.updateLastMessageStatus);

module.exports = router;