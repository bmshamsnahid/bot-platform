const express = require('express');
const router = express.Router();
const quickReplyMessageController = require('../../controller/message/quickReplyMessage');

router.post('/', quickReplyMessageController.createQuickReplyMessage);
router.get('/:id', quickReplyMessageController.getQuickReplyMessage);
router.get('/', quickReplyMessageController.getQuickReplyMessages);
router.get('/flow/:id', quickReplyMessageController.getFlowQuickReplyMessages);
router.get('/quickRepliesInfo/:id', quickReplyMessageController.getQuickRepliesInfo);
router.patch('/:id', quickReplyMessageController.updateQuickReplyMessage);
router.delete('/:id', quickReplyMessageController.deleteQuickReplyMessage);
router.patch('/firstMessageStatus/:id', quickReplyMessageController.updateFirstMessageStatus);
router.patch('/lastMessageStatus/:id', quickReplyMessageController.updateLastMessageStatus);

module.exports = router;
