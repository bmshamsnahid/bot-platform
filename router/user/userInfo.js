const express = require('express');
const router = express.Router();
const userInfoController = require('../../controller/user/userInfo');

router.post('/', userInfoController.createUserInfo)
router.get('/', userInfoController.getUserInfos);
router.get('/:id', userInfoController.getUserInfo);
router.patch('/:id', userInfoController.updateUserInfo);
router.delete('/:id', userInfoController.deleteUserInfo);

module.exports = router;
