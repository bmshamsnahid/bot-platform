const express = require('express');
const router = express.Router();
const persistentMenuController = require('../controller/persistentMenu');

router.post(['/create', '/update'], persistentMenuController.createPersistentMenu);
router.delete('/delete', persistentMenuController.deletePersistentMenu);

module.exports = router;
