const express = require('express');
const router = express.Router();
const buttonInfoController = require('../../controller/button/buttonInfo');

router.post('/', buttonInfoController.createButtonInfo);
router.get('/:id', buttonInfoController.getButtonInfo);
router.get('/', buttonInfoController.getButtonInfos);
router.patch('/:id', buttonInfoController.updateButtonInfo);
router.delete('/:id', buttonInfoController.deleteButtonInfo);

module.exports = router;