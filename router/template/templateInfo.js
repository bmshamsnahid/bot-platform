const express = require('express');
const router = express.Router();
const templateInfoController = require('../../controller/template/templateInfo');

router.post('/', templateInfoController.createTemplateInfo);
router.get('/', templateInfoController.getTemplateInfos);
router.get('/:id', templateInfoController.getTemplateInfo);
router.patch('/:id', templateInfoController.updateTemplateInfo);
router.delete('/:id', templateInfoController.deleteTemplateInfo);

module.exports = router;