const express = require('express');
const router = express.Router();
const elementInfoController = require('../../controller/element/elementInfo');

router.post('/', elementInfoController.createElementInfo);
router.get('/', elementInfoController.getElementInfos);
router.get('/:id', elementInfoController.getElementInfo);
router.patch('/:id', elementInfoController.updateElementInfo);
router.delete('/:id', elementInfoController.deleteElementInfo);

module.exports = router;
