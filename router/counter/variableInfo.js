const express = require('express');
const router = express.Router();
const variableInfoController = require('../../controller/counter/variableInfo');

router.post('/', variableInfoController.createVariableInfo);
router.get('/:id', variableInfoController.getVariableInfo);
router.get('/', variableInfoController.getVariableInfos);
router.patch('/:id', variableInfoController.updateVariableInfo);
router.delete('/:id', variableInfoController.deleteVariableInfo);

module.exports = router;
