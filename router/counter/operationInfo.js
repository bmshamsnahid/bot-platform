const express = require('express');
const router = express.Router();
const operationInfoController = require('../../controller/counter/operationInfo');

router.post('/', operationInfoController.createOperationInfo);
router.get('/', operationInfoController.getOperationInfos);
router.get('/:id', operationInfoController.getOperationInfo);
router.patch('/:id', operationInfoController.updateOperationInfo);
router.delete('/:id', operationInfoController.deleteOperationInfo);

module.exports = router;
