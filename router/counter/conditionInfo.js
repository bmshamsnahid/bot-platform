const express = require('express');
const router = express.Router();
const conditionInfoController = require('../../controller/counter/conditionInfo');

router.post('/', conditionInfoController.createConditionInfo);
router.get('/', conditionInfoController.getConditionInfos);
router.get('/:id', conditionInfoController.getConditionInfo);
router.patch('/:id', conditionInfoController.updateConditionInfo);
router.delete('/:id', conditionInfoController.deleteConditionInfo);

module.exports = router;
