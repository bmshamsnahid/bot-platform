const express = require('express');
const router = express.Router();
const getStartedButtonController = require('../controller/getStartedButton');

router.post(['/create', '/update'], getStartedButtonController.createGetStartedButton);
router.delete('/delete', getStartedButtonController.deleteGetStartedButton);

module.exports = router;
