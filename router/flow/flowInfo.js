const express = require('express');
const router = express.Router();
const flowInfoController = require('../../controller/flow/flowInfo');

router.post('/', flowInfoController.createFlowInfo);
router.get('/', flowInfoController.getFlowInfos);
router.get('/:id', flowInfoController.getFlowInfo);
router.get('/bot/:id', flowInfoController.getBotsFlow);
router.patch('/:id', flowInfoController.updateFlowInfo);
router.delete('/:id', flowInfoController.deleteFlowInfo);

module.exports = router;