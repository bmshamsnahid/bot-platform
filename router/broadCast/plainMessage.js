const express = require('express');
const router = express.Router();
const broadCastPlainMessageController = require('../../controller/broadCast/plainMessage');

router.post('/plainMessage', broadCastPlainMessageController.broadCast);

module.exports = router;
