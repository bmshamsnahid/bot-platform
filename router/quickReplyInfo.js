const express = require('express');
const router = express.Router();
const quickReplyInfoController = require('../controller/quickReplyInfo');

router.post('/', quickReplyInfoController.createQuickReplyInfo);
router.get('/:id', quickReplyInfoController.getQuickReplyInfo);
router.get('/', quickReplyInfoController.getQuickReplyInfos);
router.patch('/:id', quickReplyInfoController.updateQuickReplyInfo);
router.patch('/nextNode/:id', quickReplyInfoController.updateQuickReplyNextNodeTypeAndId);
router.delete('/:id', quickReplyInfoController.deleteQuickReplyInfo);

module.exports = router;
