const { UserLastNode } = require('../model/UserInteraction/userLastNode');
const quickReplyMessageLib = require('../messageSend/quickReply');
const textMessageLib = require('../messageSend/plainMessage');
const rawMessage = require('../messageSend/text');
const chalk = require('chalk');
const handlePayload = require('../controller/handlePayload');

module.exports = async (pageId, userId, event = null, messageText) => {
    const userLastNode = await UserLastNode.findOne({ pageId: pageId, userId: userId });
    if (userLastNode && userLastNode.lastNodeType == 'quick reply') {
        return quickReplyMessageLib(pageId, userId, userLastNode.lastNodeId);
    } else if (userLastNode && userLastNode.lastNodeType == 'plain') {
        if (userLastNode.lastNodeId) {
            return handlePayload.handlePlainMessage(userLastNode, userId, pageId, event);
        }
    } else {
        console.log(chalk.red('Invalid last node'));
    }
    return rawMessage(userId, 'You said: ' + messageText, pageId);
};
