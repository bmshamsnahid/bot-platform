const { UserConversion } = require('../model/UserInteraction/userConversion');
const config = require('config');
const moment = require('moment');
const { MessageSession } = require('../model/UserInteraction/messageSession');
const MESSAGE_SESSION_TIME = (process.env.MESSAGE_SESSION_TIME) ? process.env.MESSAGE_SESSION_TIME : config.get('sessionTime');

module.exports = async (userId, pageId, event, messageData, messageSource, messageType, messageId) => {
    
    const stepId = await UserConversion.find({ userId: userId, pageId: pageId }).countDocuments();
    const prevConversion = await UserConversion.findOne({ userId: userId, pageId: pageId, stepId: stepId });
    
    let messageSessionId = await MessageSession.find({ userId: userId, pageId: pageId }).countDocuments();
    if (!messageSessionId) {
        const initialMessageSession = new MessageSession({
            pageId: pageId,
            userId: userId,
            messageSessionId: 1
        });
        await initialMessageSession.save();
        messageSessionId = 1;
    }

    const currentTime = moment();
    let duration = 0;

    if (prevConversion) {
        const previousTime = moment(prevConversion.time);
        duration = moment.duration(currentTime.diff(previousTime)).as('milliseconds');
        if (MESSAGE_SESSION_TIME < duration) {
            messageSessionId += 1;
            const newMessageSession = new MessageSession({
                userId: userId,
                pageId: pageId,
                messageSessionId: messageSessionId
            });
            await newMessageSession.save();
        }
    }

    const userConversion = new UserConversion({
        stepId: stepId + 1,
        userId: userId,
        pageId: pageId,
        event: event || {},
        messageData: messageData || {},
        messageSource: messageSource,
        time: currentTime,
        messageSessionId: messageSessionId,
        messageType: messageType,
        messageId: messageId
    });
    await userConversion.save();
    
    return;
}