const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const error = require('../middleware/error');

const webHookRouter = require('../router/webhook');
const authorizeRouter = require('../router/authorize');
const textMessageRouter = require('../router/message/textMessage');
const botInfoRouter = require('../router/bot/botInfo');
const flowInfoRouter = require('../router/flow/flowInfo');
const quickReplyMessageRouter = require('../router/message/quickReplyMessage');
const quickReplyInfoRouter = require('../router/quickReplyInfo');
const buttonInfoRouter = require('../router/button/buttonInfo');
const elementInfoRouter = require('../router/element/elementInfo');
const templateInfoRouter = require('../router/template/templateInfo');
const getStartedButtonRouter = require('../router/getStartedButton');
const persistentMenuRouter = require('../router/persistentMenu');
const variableInfoRouter = require('../router/counter/variableInfo');
const conditionInfoRouter = require('../router/counter/conditionInfo');
const operationInfoRouter = require('../router/counter/operationInfo');
const authenticationRouter = require('../router/auth/authentication');
const userInfoRouter = require('../router/user/userInfo');
const broadCastPlainMessageRouter = require('../router/broadCast/plainMessage');

const verifyRequestSignature = require('../lib/verifyRequestSignature');

module.exports = (app) => {
    app.use(express.json());
    app.use(bodyParser.json({ verify: verifyRequestSignature }));
    app.use(morgan('dev'));

    app.use('/webhook', webHookRouter);
    app.use('/authorize', authorizeRouter);
    app.use('/api/textMessage', textMessageRouter);
    app.use('/api/botInfo', botInfoRouter);
    app.use('/api/flowInfo', flowInfoRouter);
    app.use('/api/quickReplyMessage', quickReplyMessageRouter);
    app.use('/api/quickReplyInfo', quickReplyInfoRouter);
    app.use('/api/buttonInfo', buttonInfoRouter);
    app.use('/api/elementInfo', elementInfoRouter);
    app.use('/api/templateInfo', templateInfoRouter);
    app.use('/api/getStartedButton', getStartedButtonRouter);
    app.use('/api/persistentMenu', persistentMenuRouter);
    app.use('/api/variableInfo', variableInfoRouter);
    app.use('/api/conditionInfo', conditionInfoRouter);
    app.use('/api/operationInfo', operationInfoRouter);
    app.use('/api/authentication', authenticationRouter);
    app.use('/api/userInfo', userInfoRouter);
    app.use('/api/broadCast', broadCastPlainMessageRouter);

    app.use(error);
};
