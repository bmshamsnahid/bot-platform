const mongoose = require('mongoose');
const Joi = require('joi');

const quickReplyMessageSchema = new mongoose.Schema({
    flowId: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    quick_replies: {
        type: [String],
        required: true
    },
    isFirstMessage: {
        type: Boolean,
        default: false,
    },
    isLastMessage: {
        type: Boolean,
        default: false,
    },
});

const QuickReplyMessage = mongoose.model('QuickReplyMessage', quickReplyMessageSchema);

const validateQuickReplyMessage = (quickReplyMessage) => {
    const schema = {
        flowId: Joi.objectId().required(),
        text: Joi.string().required(),
        quick_replies: Joi.array(),
        isFirstMessage: Joi.boolean(),
        isLastMessage: Joi.boolean(),
    };
    return Joi.validate(quickReplyMessage, schema);
};

exports.quickReplyMessageSchema = quickReplyMessageSchema;
exports.QuickReplyMessage = QuickReplyMessage;
exports.validateQuickReplyMessage = validateQuickReplyMessage;
