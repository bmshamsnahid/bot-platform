const Joi = require('joi');
const mongoose = require('mongoose');

const textMessageSchema = new mongoose.Schema({
    flowId: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true,
        minLength: 1,
        maxLength: 2000
    },
    metadata: {
        type: String,
        required: false,
        default: "Softograph Bot",
        maxLength: 1000
    },
    isFirstMessage: {
        type: Boolean,
        default: false,
    },
    isLastMessage: {
        type: Boolean,
        default: false,
    },
    payload: {      // nextMessageType-nextMessageId
        type: String,
        required: false,
    },
});

const textMessage = mongoose.model('TextMessage', textMessageSchema);

const validateTextMessage = (textMessage) => {
    const schema = {
        flowId: Joi.objectId().required(),
        text: Joi.string().min(1).max(2000).required(),
        metadata: Joi.string().max(1000),
        isFirstMessage: Joi.boolean(),
        isLastMessage: Joi.boolean(),
        payload: Joi.string(),
    };
    return Joi.validate(textMessage, schema);
};

exports.textMessageSchema = textMessageSchema;
exports.TextMessage = textMessage;
exports.validateTextMessage = validateTextMessage;
