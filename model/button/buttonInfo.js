const mongoose = require('mongoose');
const Joi = require('joi');

const buttonInfoSchema = new mongoose.Schema({
    name: {
        type: String,
        maxLength: 50
    },
    description: {
        type: String,
        maxLength: 2000
    },
    buttonType: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        maxLength: 100
    },
    webViewHeightRatio: {
        type: String
    },
    messengerExtension: {
        type: String,
    },
    fallBackUrl: {
        type: String,
        maxLength: 100
    },
    payload: {
        type: String,
    }
});

const ButtonInfo = mongoose.model('buttonInfo', buttonInfoSchema);

const validateButtonSchema = (buttonInfo) => {
    const schema = {
        name: Joi.string().max(50),
        description: Joi.string().max(2000),
        buttonType: Joi.string().valid('web_url', 'element_share', 'postback', 'phone_number', 'account_link', 'account_unlink', 'game_play'),
        title: Joi.string().required(),
        url: Joi.string()
            .when('buttonType', { is: 'web_url', then: Joi.required() }),
        webViewHeightRatio: Joi.string(),
        messengerExtension: Joi.boolean(),
        fallBackUrl: Joi.string().max(100)
            .when('buttonType', { is: 'web_url', then: Joi.required() }),
        payload: Joi.string().max(100)
            .when('buttonType', { is: 'postback', then: Joi.required() })
    };
    return Joi.validate(buttonInfo, schema);
};

exports.validateButtonSchema = validateButtonSchema;
exports.buttonInfoSchema = buttonInfoSchema;
exports.ButtonInfo = ButtonInfo;