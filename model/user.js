// // load the things we need
// var mongoose = require('mongoose');
// var bcrypt   = require('bcrypt-nodejs');
// const jwt = require('jsonwebtoken');
// const Joi = require('joi');

// // define the schema for our user model
// var userSchema = mongoose.Schema({
//     local: {
//         emai: String,
//         password: String
//     },
//     facebook: {
//         id: String,
//         token: String,
//         name: String,
//         email: String
//     },
//     google: {
//         id: String,
//         token: String,
//         email: String,
//         name: String
//     },
//     isAdmin: {
//         type: Boolean,
//     },
//     isPageOwner: {
//         type: Boolean,
//     }
// });

// userSchema.methods.generateAuthToken = function () {
//     const token = jwt.sign({ _id: this.id, isAdmin: this.isAdmin }, config.get('jwtPrivateKey'));
//     return token;
// };

// // generating a hash
// userSchema.methods.generateHash = function(password) {
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };

// // checking if password is valid
// userSchema.methods.validPassword = function(password) {
//     return bcrypt.compareSync(password, this.local.password);
// };

// // create the model for users and expose it to our app
// module.exports = mongoose.model('User', userSchema);
