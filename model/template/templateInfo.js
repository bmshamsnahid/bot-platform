const mongoose = require('mongoose');
const Joi = require('joi');

const templateInfoSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    description: {
        type: String,
    },
    templateType: {
        type: String,
        required: true,
    },
    topElementStyle: {
        type: String,
    },
    templateText: {
        type: String,
    },
    elements: {
        type: [String],
        default: [],
    },
    buttons: {
        type: [String],
        default: []
    }
});

const TemplateInfo = mongoose.model('templateInfo', templateInfoSchema);

const validateTemplateInfo = (templateInfo) => {
    const schema = {
        name: Joi.string(),
        description: Joi.string(),
        templateType: Joi.string().valid('generic', 'list', 'button', 'media').required(),
        topElementStyle: Joi.string().valid('compact', 'large')
            .when('templateType', { is: 'list', then: Joi.required() }),
        templateText: Joi.string().min(1)
            .when('templateType', { is: 'button', then: Joi.required() }),
        elements: Joi.array().items(Joi.objectId()),
        buttons: Joi.array().items(Joi.objectId())
    };
    return Joi.validate(templateInfo, schema);
};

exports.templateInfoSchema = templateInfoSchema;
exports.TemplateInfo = TemplateInfo;
exports.validateTemplateInfo = validateTemplateInfo;
