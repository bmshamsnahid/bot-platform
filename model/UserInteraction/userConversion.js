const mongoose = require('mongoose');
const Joi = require('joi');

const userConversionSchema = new mongoose.Schema({
    stepId: {
        type: Number
    },
    userId: {
        type: String
    },
    pageId: {
        type: String
    },
    questionId: {
        type: String
    },
    responseId: {
        type: String
    },
    time: {
        type: Date,
        default: new Date()
    },
    timeDiff: {
        type: Date
    },
    messageSource: {
        type: String
    },
    time: {
        type: Date
    },
    messageSessionId: {
        type: Number
    },
    messageType: {
        type: String
    },
    messageId: {
        type: String
    },
    event: {},
    messageData: {}
});

const UserConversion = mongoose.model('UserConversion', userConversionSchema);

module.exports.userConversionSchema = userConversionSchema;
module.exports.UserConversion = UserConversion;
