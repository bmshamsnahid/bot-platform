const mongoose = require('mongoose');
const Joi = require('joi');

const userQaSchema = new mongoose.Schema({
    previousNodeType: {
        type: String,
        required: true,
    },
    previousNodeId: {
        type: String,
        required: true,
    },
    event: {},
    facebookUser: {},
    question: {
        type: String,
        required: true,
    },
    answer: {
        type: String,
        required: true,
    },
    nextNodeType: {
        type: String,
        required: false,
    },
    nextNodeId: {
        type: String,
        required: false,
    },
    userId: {
        type: String,
        required: true,
    },
    pageId: {
        type: String,
        required: true,
    },
    isLogged: {
        type: Boolean,
        default: false,
    },
});

const UserQa = mongoose.model('userQa', userQaSchema);

const validateUserQaSchema = (userQaSchema) => {
    const schema = {
        previousNodeType: Joi.string().required(),
        previousNodeId: Joi.objectId().required(),
        event: Joi.any().required(),
        facebookUser: Joi.any().required(),
        question: Joi.string().required(),
        answer: Joi.string().required(),
        nextNodeType: Joi.string(),
        nextNodeId: Joi.objectId(),
        userId: Joi.string().required(),
        pageId: Joi.string().required(),
        isLogged: Joi.boolean(),
    };
    return Joi.validate(userQaSchema, schema);
};

module.exports.userQaSchema = userQaSchema;
module.exports.UserQa = UserQa;
module.exports.validateUserQaSchema = validateUserQaSchema;
