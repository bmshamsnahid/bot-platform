const mongoose = require('mongoose');
const Joi = require('joi');

const userLastNodeSchema = new mongoose.Schema({
    pageId: {
        type: String,
        required: true,
    },
    userId: {
        type: String,
        required: true,
    },
    lastNodeType: {
        type: String,
        required: true,
    },
    lastNodeId: {
        type: String,
        required: true,
    },
    time: {
        type: Date,
        required: true,
    },
    event: {},
    isEnable: {
        type: Boolean,
        required: true,
    }
});

const UserLastNode = mongoose.model('userLastNode', userLastNodeSchema);

const validateUserLastNode = (userLastNode) => {
    const schema = {
        pageId: Joi.string().required(),
        userId: Joi.string().required(),
        lastNodeType: Joi.string().required(),
        lastNodeId: Joi.string().required(),
        time: Joi.any().required(),
        event: Joi.any(),
        isEnable: Joi.any().required(),
    };
    return Joi.validate(userLastNode, schema);
};

module.exports.userLastNodeSchema = userLastNodeSchema;
module.exports.UserLastNode = UserLastNode;
module.exports.validateUserLastNode = validateUserLastNode;
