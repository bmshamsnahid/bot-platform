const mongoose = require('mongoose');
const Joi = require('joi');

const messageSessionSchema = new mongoose.Schema({
    userId: {
        type: String
    },
    pageId: {
        type: String
    },
    messageSessionId: {
        type: Number
    }
});

const MessageSession = mongoose.model('messageSession', messageSessionSchema);

module.exports.messageSessionSchema = messageSessionSchema;
module.exports.MessageSession = MessageSession;