const mongoose = require('mongoose');
const Joi = require('joi');

const logSchema = new mongoose.Schema({
    question: {
        type: String,
        required: true,
    },
    answer: {
        type: String,
        required: true,
    },
    qaId: {
        type: String,
        required: true,
    }
});

const userLogSchema = new mongoose.Schema({
    log: [logSchema],
    pageId: {
        type: String,
        required: true,
    },
    userId: {
        type: String,
        required: true,
    },
});

const validateUserLog = (userLog) => {
    const schema = {
        log: Joi.any(),
        pageId: Joi.string().required(),
        userId: Joi.string().required(),
    };

    return Joi.validate(userLog, schema);
};

const UserLog = mongoose.model('userLog', userLogSchema);

module.exports.userLogSchema = userLogSchema;
module.exports.validateUserLog = validateUserLog;
module.exports.UserLog = UserLog;
