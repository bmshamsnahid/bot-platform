const mongoose = require('mongoose');
const Joi = require('joi');

const conditionInfoSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    description: {
        type: String,
    },
    variableInfo1: {
        type: String,
        required: true,
    },
    variableInfo2: {
        type: String,
        required: true,
    },
    condition: {
        type: String,
        required: true,
    },
    trueResultPayload: {
        type: String
    },
    falseResultPayload: {
        type: String,
    },
    defaultPaylaod: {
        type: String,
    }
});

const ConditionInfo = mongoose.model('ConditionInfo', conditionInfoSchema);

const validateCondition = (conditionInfo) => {
    const schema = {
        name: Joi.string(),
        description: Joi.string(),
        variableInfo1: Joi.objectId().required(),
        variableInfo2: Joi.objectId().required(),
        condition: Joi.valid('==', '=>', '<=').required(),
        trueResultPayload: Joi.string().regex(/^(image|gif|audio|video|file|button|generic|receipt|quick reply|read receipt|plain|array|plain|flow|condition)-(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/),
        falseResultPayload: Joi.string().regex(/^(image|gif|audio|video|file|button|generic|receipt|quick reply|read receipt|plain|array|plain|flow|condition)-(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/),
        defaultPaylaod: Joi.string().regex(/^(image|gif|audio|video|file|button|generic|receipt|quick reply|read receipt|plain|array|plain|flow|condition)-(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/)
    };
    return Joi.validate(conditionInfo, schema);
};

exports.conditionInfoSchema = conditionInfoSchema;
exports.ConditionInfo = ConditionInfo;
exports.validateCondition = validateCondition;
