const mongoose = require('mongoose');
const Joi = require('joi');

const variableInfoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
    },
    variableType: {
        type: String,
        required: true,
    },
    initialValue: {
        type: String,
        required: true,
    },
    value: {
        type: String,
    },
    botId: {
        type: String,
        required: true,
    },
    fbUserId: {
        type: String,
        default: 'model',
    },
});

const VariableInfo = mongoose.model('VariableInfo', variableInfoSchema);

const validateVariableInfo = (variableInfo) => {
    const schema = {
        name: Joi.string().min(3).required(),
        description: Joi.string(),
        variableType: Joi.string().valid("string", "number").required(),
        initialValue: Joi.string().required(),
        value: Joi.string(),
        botId: Joi.objectId().required(),
        fbUserId: Joi.string()
    };
    return Joi.validate(variableInfo, schema);
};

exports.variableInfoSchema = variableInfoSchema;
exports.VariableInfo = VariableInfo;
exports.validateVariableInfo = validateVariableInfo;
