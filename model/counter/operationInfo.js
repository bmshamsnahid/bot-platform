const mongoose = require('mongoose');
const Joi = require('joi');

const operationInfoSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    description: {
        type: String,
    },
    variableInfo1: {
        type: String,
        required: true,
    },
    variableInfo2: {
        type: String,
        required: true,
    },
    action: {
        type: String,
        required: true,
    },
    parentId: {
        type: String,
        required: true
    }
});

const OperationInfo = mongoose.model('OperationInfo', operationInfoSchema);

const validateOperationInfo = (operationInfo) => {
    const schema = {
        name: Joi.string().min(3),
        description: Joi.string(),
        variableInfo1: Joi.objectId().required(),
        variableInfo2: Joi.objectId().required(),
        action: Joi.string().valid('increment').required(),
        parentId: Joi.objectId().required()
    };
    return Joi.validate(operationInfo, schema);
};

exports.operationInfoSchema = operationInfoSchema;
exports.OperationInfo = OperationInfo;
exports.validateOperationInfo = validateOperationInfo;
