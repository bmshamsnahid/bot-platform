const Joi = require('joi');
const mongoose = require('mongoose');
const config = require('config');

const APP_SECRET = (process.env.MESSENGER_APP_SECRET) ? process.env.MESSENGER_APP_SECRET : config.get('appSecret');
const SERVER_URL = (process.env.MESSENGER_SERVER_URL) ? process.env.MESSENGER_SERVER_URL : config.get('serverURL');
const APP_ID = (process.env.MESSENGER_APP_ID) ? process.env.MESSENGER_APP_ID : config.get('appId');
const WEB_HOOK_Token = (process.env.MESSENGER_VALIDATION_TOKEN) ? (process.env.MESSENGER_VALIDATION_TOKEN) : config.get('validationToken');

const botInfoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minLength: 1,
        maxLength: 20
    },
    description: {
        type: String,
        required: true,
        minLength: 10,
        maxLength: 2000
    },
    text: {
        type: String,
        minLength: 1,
        maxLength: 2000
    },
    appId: {
        type: String,
        minLength: 5,
        maxLength: 30,
        required: true
    },
    appSecret: {
        type: String,
        minLength: 20,
        maxLength: 500,
        required: true
    },
    webHookToken: {
        type: String,
        minLength: 10,
        maxLength: 25,
        required: true
    },
    pageId: {
        type: String,
        required: true,
        minLength: 1,
        unique: true
    },
    pageName: {
        type: String,
        required: true,
        minLength: 1,
        unique: true
    },
    pageAccessToken: {
        type: String,
        required: true,
        minLength: 50,
        maxLength: 500,
        unique: true
    },
    webHookUrl: {
        type: String,
        required: true
    },
    complexity: {
        type: String,
        required: true
    },
    ownerId: {
        type: String,
        required: false
    }
});

const BotInfo = mongoose.model('BotInfoSchema', botInfoSchema);

const validateBotInfo = (botInfo) => {
    const schema = {
       name : Joi.string().min(1).max(20).required(),
       description : Joi.string().min(10).max(2000).required(),
       text : Joi.string().min(1).max(2000),
       appId : Joi.string().min(5).max(30).required(),
       appSecret : Joi.string().min(20).max(500).required(),
       webHookToken : Joi.string().min(10).max(25).required(),
       pageAccessToken : Joi.string().min(50).max(500).required(),
       pageId : Joi.string().min(1).required(),
       pageName : Joi.string().min(1).required(),
       webHookUrl : Joi.string().required(),
       complexity: Joi.any().allow('Advance', 'Basic').required(),
       ownerId: Joi.objectId()
    };
    return Joi.validate(botInfo, schema);
};

const structureBotInfo = (botInfo) => {
    if (botInfo.complexity === 'Basic') {
        botInfo.appId = APP_ID;
        botInfo.appSecret = APP_SECRET;
        botInfo.webHookToken = WEB_HOOK_Token;
        botInfo.webHookUrl = SERVER_URL + 'webhook';
    } else {
        botInfo.webHookUrl = SERVER_URL + 'webhook/' + botInfo.pageId;
    }
    return botInfo;
};

exports.botInfoSchema = botInfoSchema;
exports.BotInfo = BotInfo;
exports.validateBotInfo = validateBotInfo;
exports.structureBotInfo = structureBotInfo;
