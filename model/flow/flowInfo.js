const mongoose = require('mongoose');
const Joi = require('joi');

const flowInfoSchema = new mongoose.Schema({
    triggerKeyword: {
        type: [String],
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    text: {
        type: String,
        required: false
    },
    payload: {
        type: String,
        required: false
    },
    isActive: {
        type: Boolean,
        default: false
    },
    initialFlow: {
        type: Boolean,
        default: false,
    },
    botsId: {
        type: [String]
    },
    pagesId: {
        type: [String]
    },
    ownerId: {
        type: String,
        required: false
    }
});

const FlowInfo = mongoose.model('FlowInfo', flowInfoSchema);

const validateFlowInfo = (flowInfo) => {
    const schema = {
        triggerKeyword: Joi.array().min(1).items(Joi.string()),
        name: Joi.string().min(2).required(),
        description: Joi.string(),
        text: Joi.string(),
        payload: Joi.any(),
        isActive: Joi.boolean(),
        initialFlow: Joi.boolean(),
        botsId: Joi.array().items(Joi.objectId()),
        pagesId: Joi.array().items(Joi.string()),
        ownerId: Joi.string()
    };
    return Joi.validate(flowInfo, schema);
};

exports.flowInfoSchema = flowInfoSchema;
exports.FlowInfo = FlowInfo;
exports.validateFlowInfo = validateFlowInfo;
