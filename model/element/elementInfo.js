const mongoose = require('mongoose');
const Joi = require('joi');

const elementInfoSchema = new mongoose.Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    elementType: {
        type: String,
        required: true
    }, 
    title: {
        type: String,
    }, 
    imageUrl: {
        type: String,
    },
    subTitle: {
        type: String
    },
    defaultActionType: {
        type: String
    },
    defaultActionUrl: {
        type: String
    },
    defauktActionWebViewHeightRatio: {
        type: String
    },
    buttonsId: {
        type: [String]
    }
});

const ElementInfo = mongoose.model('elementInfo', elementInfoSchema);

const validateElementInfo = (elementInfo) => {
    const schema = {
        name: Joi.string(),
        description: Joi.string(),
        elementType: Joi.any().valid('generic', 'list').required(),
        title: Joi.string().required(),
        imageUrl: Joi.string()
            .when('elementType', { is: 'generic', then: Joi.required() }),
        subTitle: Joi.string().required(),
        defaultActionType: Joi.string().valid('web_url')
            .when('elementType', { is: 'generic', then: Joi.required() }),
        defaultActionUrl: Joi.string()
            .when('elementType', { is: 'generic', then: Joi.required() }),
        defauktActionWebViewHeightRatio: Joi.string().valid('compact', 'tall', 'full')
           .when('elementType', { is: 'generic', then: Joi.required() }),
        buttonsId: Joi.array().items(Joi.objectId())
    };
    return Joi.validate(elementInfo, schema);
};

exports.ElementInfo = ElementInfo;
exports.elementInfoSchema = elementInfoSchema;
exports.validateElementInfo = validateElementInfo;
