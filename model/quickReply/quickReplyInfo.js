const mongoose = require('mongoose');
const Joi = require('joi');

const quickReplyInfoSchema = new mongoose.Schema({
    content_type: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: false
    },
    payload: {
        type: String,
        required: false,
        unique: false,
        default: 'dummy payload' // nextNodeType-nextNodeId-previousNodeType-previousNodeId
    }
});

const QuickReplyInfo = mongoose.model('QucikReplyInfo', quickReplyInfoSchema);

const validateQuickReplyInfo = (quickReplyInfo) => {
    const schema = {
        content_type: Joi.any().valid('text', 'location', 'user_phone_number', 'user_email').required(),
        title: Joi.string(),
        // payload: Joi.string().regex(/^(image|gif|audio|video|file|button|generic|receipt|quick reply|read receipt|plain|array|plain|flow|condition)-(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/)
        payload: Joi.string(),
    };
    return Joi.validate(quickReplyInfo, schema);
};

exports.quickReplySchema = quickReplyInfoSchema;
exports.QuickReplyInfo = QuickReplyInfo;
exports.validateQuickReplyInfo = validateQuickReplyInfo;
