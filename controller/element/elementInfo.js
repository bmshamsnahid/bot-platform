const { ElementInfo, validateElementInfo } = require('../../model/element/elementInfo');
const _ = require('lodash');

const createElementInfo = async (req, res, next) => {
    const { error } = validateElementInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });
    const elementInfo = new ElementInfo(_.pick(req.body, ['name', 'description', 'elementType', 'title', 'imageUrl', 'subTitle', 'defaultActionType', 'defaultActionUrl', 'defauktActionWebViewHeightRatio', 'buttonsId']));
    const result = await elementInfo.save();
    return res.status(200).json({ success: true, data: result });
};

const getElementInfo = async (req, res, next) => {
    const elementInfo = await ElementInfo.findById(req.params.id);
    if (!elementInfo) return res.status(400).json({ success: false, message: 'Invalid element id' });
    return res.status(200).json({ success: true, data: elementInfo });
};

const getElementInfos = async (req, res, next) => {
    const elementInfos = await ElementInfo.find();
    return res.status(200).json({ success: true, data: elementInfos });
};

const updateElementInfo = async (req, res, next) => {
    let elementInfo = await ElementInfo.findById(req.params.id);
    if (!elementInfo) return res.status(400).json({ success: false, message: 'Invalid element id' });
    
    elementInfo.elementType = req.body.elementType || elementInfo.elementType;
    elementInfo.title = req.body.title || elementInfo.title;
    elementInfo.imageUrl = req.body.imageUrl || elementInfo.imageUrl;
    elementInfo.subTitle = req.body.subTitle || elementInfo.subTitle;
    elementInfo.defaultActionType = req.body.defaultActionType || elementInfo.defaultActionType;
    elementInfo.defaultActionUrl = req.body.defaultActionUrl || elementInfo.defaultActionUrl;
    elementInfo.defauktActionWebViewHeightRatio = req.body.defauktActionWebViewHeightRatio || elementInfo.defauktActionWebViewHeightRatio;
    elementInfo.buttonsId = req.body.buttonsId || elementInfo.buttonsId;
    
    const { error }  = validateElementInfo(_.pick(elementInfo, ['name', 'description', 'elementType', 'title', 'imageUrl', 'subTitle', 'defaultActionType', 'defaultActionUrl', 'defauktActionWebViewHeightRatio', 'buttonsId']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await elementInfo.save();
    return res.status(200).json({ success: true, data: result });
};

const deleteElementInfo = async (req, res, next) => {
    const elementInfo = await ElementInfo.findByIdAndRemove(req.params.id);
    if (!elementInfo) return res.status(400).json({ success: false, message: 'Invalid element info id' });
    return res.status(200).json({ success: true, data: elementInfo });
};

module.exports = {
    createElementInfo,
    getElementInfo,
    getElementInfos,
    updateElementInfo,
    deleteElementInfo
};
