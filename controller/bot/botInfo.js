const { BotInfo, validateBotInfo, structureBotInfo } = require('../../model/bot/botInfo');
const _ = require('lodash');

const createBotInfo = async (req, res) => {
    const structuredBotInfo = structureBotInfo(req.body);

    const { error } = validateBotInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const botInfo = new BotInfo(structuredBotInfo);
    const result = await botInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const getBotInfo = async (req, res, next) => {
    const botInfo = await BotInfo.findById(req.params.id);
    if (!botInfo) return res.status(400).json({ success: false, message: 'Invalid bot info id' });
    return res.status(201).json({ success: true, data: botInfo });
};

const getAllBotInfo = async (req, res, next) => {
    const botInfos = await BotInfo.find();
    return res.status(201).json({ success: true, data: botInfos });
};

const updateBotInfo = async (req, res, next) => {
    let botInfo = await BotInfo.findById(req.params.id);
    if (!botInfo) return res.status(400).json({ success: false, message: 'Invalide Id' });
    
    botInfo.name = req.body.name || botInfo.name;
    botInfo.description = req.body.description || botInfo.description;
    botInfo.text = req.body.text || botInfo.text;
    botInfo.appId = req.body.appId || botInfo.appId;
    botInfo.appSecret = req.body.appSecret || botInfo.appSecret;
    botInfo.webHookToken = req.body.webHookToken || botInfo.webHookToken;
    botInfo.pageId = req.body.pageId || botInfo.pageId;
    botInfo.pageName = req.body.pageName || botInfo.pageName;
    botInfo.pageAccessToken = req.body.pageAccessToken || botInfo.pageAccessToken;
    botInfo.webHookUrl = req.body.webHookUrl || botInfo.webHookUrl;
    botInfo.complexity = req.body.complexity || botInfo.complexity;
    botInfo.ownerId = req.body.ownerId || botInfo.ownerId;

    const { error } = validateBotInfo(_.pick(botInfo, ['name', 'description', 'text', 'appId', 'appSecret', 'webHookToken', 'pageId', 'paheName', 'pageAccessToken', 'webHookUrl', 'complexity', 'ownerId' ]));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await botInfo.save();
    return res.send(result);
};

const deleteBotInfo = async (req, res, next) => {
    const result = await BotInfo.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).json({ success: false, message: 'Invalid Id' });
    return res.status(201).json({ success: true, message: 'Successfully delete the bot info.' });
};

module.exports = {
  createBotInfo,
  getBotInfo,
  getAllBotInfo,
  updateBotInfo,
  deleteBotInfo
};