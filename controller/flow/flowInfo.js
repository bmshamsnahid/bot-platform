const { FlowInfo, validateFlowInfo } = require('../../model/flow/flowInfo');
const _ = require('lodash');

const createFlowInfo = async (req, res, next) => {

    const { error } = validateFlowInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });
    
    const flowInfo = new FlowInfo(_.pick(req.body, ['triggerKeyword', 'name', 'description', 'text', 'isActive', 'initialFlow', 'botsId', 'pagesId', 'ownerId']));
    const result = await flowInfo.save();

    return res.status(201).json({ success: true, data: result });
};

const getFlowInfo = async (req, res, next) => {
    const flowInfo = await FlowInfo.findById(req.params.id);
    if (!flowInfo) return res.status(400).json({ success: false, messge: 'Invalid flow id.' });
    
    return res.status(201).json({ success: false, data: flowInfo });
};

const getFlowInfos = async (req, res, next) => {
    const flowInfos = await FlowInfo.find();
    return res.status(201).json({ success: true, data: flowInfos });
};

const getBotsFlow = async (req, res, next) => {
    const flowInfos = await FlowInfo.find({ botsId: { $in: [req.params.id] } });
    return res.status(201).json({ success: false, data: flowInfos });
};

const updateFlowInfo = async (req, res, next) => {
    let flowInfo = await FlowInfo.findById(req.params.id);
    if (!flowInfo) return res.status(400).json({ success: false, message: 'Invalid flow Id' });

    flowInfo.triggerKeyword = req.body.triggerKeyword || flowInfo.triggerKeyword;
    flowInfo.name = req.body.name || flowInfo.name;
    flowInfo.description = req.body.description || flowInfo.description;
    flowInfo.text = req.body.text || flowInfo.text;
    flowInfo.payload = req.body.payload || flowInfo.payload;
    flowInfo.isActive = req.body.isActive || flowInfo.isActive;
    flowInfo.initialFlow = req.body.initialFlow || flowInfo.initialFlow;
    flowInfo.botsId = req.body.botsId || flowInfo.botsId;
    flowInfo.pagesId = req.body.pagesId || flowInfo.pagesId;
    flowInfo.ownerId = req.body.ownerId || flowInfo.ownerId;

    const { error } = validateFlowInfo(_.pick(flowInfo, [ 'triggerKeyword', 'name', 'description', 'text', 'payload', 'isActive', 'initialFlow', 'botsId', 'pagesId', 'ownerId']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await flowInfo.save();

    return res.status(201).json({ success: true, data: result });
};

const deleteFlowInfo = async (req, res, next) => {
    const flowInfo = await FlowInfo.findByIdAndRemove(req.params.id);
    if (!flowInfo) return res.status(400).json({ success: false, message: 'Invalid flow Id' });

    return res.status(201).json({ success: true, data: flowInfo });
};

module.exports = {
    createFlowInfo,
    getFlowInfo,
    getFlowInfos,
    getBotsFlow,
    updateFlowInfo,
    deleteFlowInfo
};