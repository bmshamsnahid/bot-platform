const { TextMessage, validateTextMessage } = require('../../model/message/text');
const _ = require('lodash');

const createMessage = async (req, res, next) => {
    const { error } = validateTextMessage(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const textMessage = new TextMessage(_.pick(req.body, ['flowId', 'text', 'metadata', 'payload']));
    const result = await textMessage.save();
    res.status(200).json({
        success: true,
        message: "Successfully created the plain message",
        data: result
    })
};

const getMessage = async (req, res, next) => {
    const textMessage = await TextMessage.findById(req.params.id);
    if (!textMessage) return res.status(400).json({ success: false, message: 'Invalid text message id' });

    return res.status(200).json({ success: true, data: textMessage });
};

const getMessages = async (req, res, next) => {
    const textMessages = await TextMessage.find({});
    return res.status(200).json({ success: true, data: textMessages });
};

const updateMessage = async (req, res, next) => {
    let textMessage = await TextMessage.findById(req.params.id);
    if (!textMessage) return res.status(400).json({ success: false, message: 'Invalid text message id' });

    textMessage.flowId = req.body.flowId || textMessage.flowId;
    textMessage.text = req.body.text || textMessage.text;
    textMessage.metadata = req.body.metadata || textMessage.metadata;
    textMessage.payload = req.body.payload || textMessage.payload;

    const { error } = validateTextMessage(_.pick(textMessage, ['flowId', 'text', 'metadata', 'payload']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await textMessage.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteMessage = async (req, res, next) => {
    const textMessage = await TextMessage.findByIdAndRemove(req.params.id);
    if (!textMessage) return res.status(400).json({ success: false, message: 'Invalid text message id' });

    return res.status(200).json({ success: true, data: textMessage });
};

const updateFirstMessageStatus = async (req, res, next) => {
    let textMessage = await TextMessage.findById(req.params.id);
    if (!textMessage) return res.status(400).json({ success: false, message: 'Invalid text message id' });

    textMessage.isFirstMessage = !!req.body.isFirstMessage;

    const result = await textMessage.save();

    return res.status(200).json({ success: true, data: result });
};

const updateLastMessageStatus = async (req, res, next) => {
    let textMessage = await TextMessage.findById(req.params.id);
    if (!textMessage) return res.status(400).json({ success: false, message: 'Invalid text message id' });

    textMessage.isLastMessage = !!req.body.isLastMessage;

    const result = await textMessage.save();

    return res.status(200).json({ success: true, data: result });
};

module.exports = {
  createMessage,
  getMessage,
  getMessages,
  updateMessage,
  deleteMessage,
  updateFirstMessageStatus,
  updateLastMessageStatus,
};
