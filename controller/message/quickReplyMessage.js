const { QuickReplyMessage, validateQuickReplyMessage} = require('../../model/message/quickReplyMessage');
const quickReplyService = require('../../service/quickReply/quickReplyInfo');
const _ = require('lodash');
const chalk = require('chalk');

const createQuickReplyMessage = async (req, res, next) => {
    
    const quick_replies = req.body.quick_replies;
    const quickRepliesInfoResult = await quickReplyService.createMultipleQuickReplies(quick_replies);
    
    if (quickRepliesInfoResult.error) return res.status(400).json({ success: false, messsage: error.details[0].message });
    
    const quickReplyMessageSkeleton = {
        flowId: req.body.flowId,
        text: req.body.text,
        quick_replies: quickRepliesInfoResult.quickRepliesId
    }

    let { error } = validateQuickReplyMessage(quickReplyMessageSkeleton);
    if (error) return res.status(400).json({ success: false, message: error.details });

    const quickReplyMessage = new QuickReplyMessage(quickReplyMessageSkeleton);
    let result = await quickReplyMessage.save();

    const previousNodeType = 'quick reply';
    const previousNodeId = result._id;

    const resultError = await quickReplyService.updateQuickRepliesPreviousNodeAndId(quickRepliesInfoResult.quickRepliesId, previousNodeType, previousNodeId);

    if (resultError.error) return res.status(400).json({ success: false, message: result.error });

    return res.status(200).json({ success: true, data: result });
};

const getQuickReplyMessage = async (req, res, next) => {
    const quickReplyMessage = await QuickReplyMessage.findById(req.params.id);
    if (!quickReplyMessage) return res.status(400).json({ success: false, message: 'Invalid quick reply message id' });
    
    return res.status(201).json({ success: true, data: quickReplyMessage });
};

const getQuickReplyMessages = async (req, res, next) => {
    const quickReplyMessages = await QuickReplyMessage.find();
    if (!quickReplyMessages) return res.status(400).json({ success: false, message: 'Internal operation error' });
    
    return res.status(201).json({ success: true, data: quickReplyMessages });
};

const getFlowQuickReplyMessages = async (req, res, next) => {
    const quickReplyMessages = await QuickReplyMessage.find({ flowId: req.params.id });
    return res.status(201).json({ success: true, data: quickReplyMessages });
};

const getQuickRepliesInfo = async (req, res, next) => {
    let quickReplyInfos = [];

    const quickReplyMessage = await QuickReplyMessage.findById(req.params.id);
    if (!quickReplyMessage) return res.status(400).json({ success: false, message: 'Invalid quick reply message id' });

    const quickReplyInfosId = quickReplyMessage.quick_replies;
    
    for (let index=0; index<quickReplyInfosId.length; index++) {
        const {err, data} = await quickReplyService.getQuickReplyInfo(quickReplyInfosId[index]);
        if (err) return res.status(400).json({ success: false, err: err });
        quickReplyInfos.push(data);
    }

    return res.status(200).json({ success: true, data: quickReplyInfos });
};

const updateQuickReplyMessage = async (req, res, next) => {
    let quickReplyMessage = await QuickReplyMessage.findById(req.params.id);
    if (!quickReplyMessage) return res.status(400).json({ success: false, message: 'Invalid quick reply message id' });

    quickReplyMessage.flowId = req.body.flowId || quickReplyMessage.flowId;
    quickReplyMessage.text = req.body.text || quickReplyMessage.text;
    quickReplyMessage.quick_replies = req.body.quick_replies || quickReplyMessage.quick_replies;

    const { error } = validateQuickReplyMessage(_.pick(quickReplyMessage, [ 'flowId', 'text', 'quick_replies']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await quickReplyMessage.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteQuickReplyMessage = async (req, res, next) => {
    let result = await QuickReplyMessage.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).json({ success: false, message: 'Invalid quick reply message id' });

    return res.status(200).json({ success: true, data: result });
};

const updateFirstMessageStatus = async (req, res, next) => {
    let quickReplyMessage = await QuickReplyMessage.findById(req.params.id);
    if (!quickReplyMessage) return res.status(400).json({ success: false, message: 'Invalid quick reply message id' });

    quickReplyMessage.isFirstMessage = !!req.body.isFirstMessage;

    const result = await quickReplyMessage.save();

    return res.status(200).json({ success: true, data: result });
};

const updateLastMessageStatus = async (req, res, next) => {
    let quickReplyMessage = await QuickReplyMessage.findById(req.params.id);
    if (!quickReplyMessage) return res.status(400).json({ success: false, message: 'Invalid quick reply message id' });

    quickReplyMessage.isLastMessage = !!req.body.isLastMessage;

    const result = await quickReplyMessage.save();

    return res.status(200).json({ success: true, data: result });
};

module.exports = {
    createQuickReplyMessage,
    getQuickReplyMessage,
    getQuickReplyMessages,
    getFlowQuickReplyMessages,
    getQuickRepliesInfo,
    updateQuickReplyMessage,
    deleteQuickReplyMessage,
    updateFirstMessageStatus,
    updateLastMessageStatus,
};
