const { TemplateInfo, validateTemplateInfo } = require('../../model/template/templateInfo');
const _ = require('lodash');

const createTemplateInfo = async (req, res, next) => {
    const { error } = validateTemplateInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const templateInfo = new TemplateInfo(_.pick(req.body, ['name', 'description', 'templateType', 'topElementStyle', 'templateText', 'elements', 'buttons']));
    const result = await templateInfo.save();
    
    return res.status(200).json({ success: true, data: result });
};

const getTemplateInfos = async (req, res, next) => {
    const templateInfos = await TemplateInfo.find();
    return res.status(200).json({ success: true, data: templateInfos });
};

const getTemplateInfo = async (req, res, next) => {
    const templateInfo = await TemplateInfo.findById(req.params.id);
    if (!templateInfo) return res.status(400).json({ success: false, message: 'Invalid template info id' });
    return res.status(200).json({ success: true, data: templateInfo });
};

const updateTemplateInfo = async (req, res, next) => {
    let templateInfo = await TemplateInfo.findById(req.params.id);
    if (!templateInfo) return res.status(400).json({ success: false, message: 'Invalid template info id' });

    templateInfo.name = req.body.name || templateInfo.name;
    templateInfo.description = req.body.description || templateInfo.description;
    templateInfo.templateType = req.body.templateType || templateInfo.templateType;
    templateInfo.topElementStyle = req.body.topElementStyle || templateInfo.topElementStyle;
    templateInfo.templateText = req.body.templateText || templateInfo.templateText;
    templateInfo.elements = req.body.elements || templateInfo.elements;
    templateInfo.buttons = req.body.buttons || templateInfo.buttons;

    const { error } = validateTemplateInfo(_.pick(templateInfo, ['name', 'description', 'templateType', 'topElementStyle', 'templateText', 'elements', 'buttons']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await templateInfo.save();
    return res.status(200).json({ success: true, data: result });
};

const deleteTemplateInfo = async (req, res, next) => {
    const templateInfo = await TemplateInfo.findByIdAndRemove(req.params.id);
    if (!templateInfo) return res.status(400).json({ success: false, message: 'Invalid id' });
    return res.status(200).json({ success: true, data: templateInfo });
};

module.exports = {
    createTemplateInfo,
    getTemplateInfos,
    getTemplateInfo,
    updateTemplateInfo,
    deleteTemplateInfo
};
