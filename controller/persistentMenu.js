const request = require('../helper/requestPromise');
const baseUrl = require('../baseUrl');
const { BotInfo } = require('../model/bot/botInfo');

const createPersistentMenu = async (req, res, next) => {
    const botInfo = await BotInfo.findOne({ pageId: req.body.pageId });
    if (!botInfo) return res.status(400).json({ success: false, message: 'Invalid page Id' });
    
    const url = baseUrl.persistentMenu + '?access_token=' + botInfo.persistentMenu;

    const  messageData = {
        "persistent_menu": req.body.persistentMenu
    };

    const options = {
        uri: url,
        qs: { access_token: botInfo.pageAccessToken },
        method: 'post',
        json: messageData
    };
    
    const response = await request(options);

    res.status(200).json({ success: true, data: response });
};

const deletePersistentMenu = async (req, res, next) => {
    const botInfo = await BotInfo.findOne({ pageId: req.query.pageId });
    if (!botInfo) return res.status(400).json({ success: false, message: 'Invalid page Id' });

    const url = baseUrl.persistentMenu + '?access_token=' + botInfo.pageAccessToken;
    
    const messageData = {
        "fields":[
            "persistent_menu"
        ]
    };

    const options = {
        uri: url,
        qs: { access_token: botInfo.pageAccessToken },
        method: 'delete',
        json: messageData
    };
    
    const response = await request(options);

    res.status(200).json({ success: true, data: response });
};

module.exports = {
    createPersistentMenu,
    deletePersistentMenu
};
