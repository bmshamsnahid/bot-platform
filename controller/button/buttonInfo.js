const { ButtonInfo, validateButtonSchema } = require('../../model/button/buttonInfo');
const _ = require('lodash');

const createButtonInfo = async (req, res, next) => {
    const { error } = validateButtonSchema(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const buttonInfo = new ButtonInfo(_.pick(req.body, ['name', 'description', 'buttonType', 'title', 'url', 'webViewHeightRatio', 'messengerExtension', 'fallBackUrl', 'payload']));
    const result = await buttonInfo.save();
    return res.status(200).json({ success: true, data: result });
};

const getButtonInfo = async (req, res, next) => {
    const buttonInfo = await ButtonInfo.findById(req.params.id);
    if (!buttonInfo) return res.status(400).json({ success: false, message: 'Invalid button info' });
    return res.status(200).json({ success: true, data: buttonInfo });
};

const getButtonInfos = async (req, res, next) => {
    const result = await ButtonInfo.find();
    return res.status(200).json({ success: true, data: result });
};

const updateButtonInfo = async (req, res, next) => {
    let buttonInfo = await ButtonInfo.findById(req.params.id);
    if (!buttonInfo) return res.status(400).json({ success: false, message: 'Invalid button info' });

    buttonInfo.name = req.body.name || buttonInfo.name;
    buttonInfo.description = req.body.description || buttonInfo.description;
    buttonInfo.buttonType = req.body.buttonType || buttonInfo.buttonType;
    buttonInfo.title = req.body.title || buttonInfo.title;
    buttonInfo.url = req.body.url || buttonInfo.url;
    buttonInfo.webViewHeightRatio = req.body.webViewHeightRatio || buttonInfo.webViewHeightRatio;
    buttonInfo.messengerExtension = req.body.messengerExtension || buttonInfo.messengerExtension;
    buttonInfo.fallBackUrl = req.body.fallBackUrl || buttonInfo.fallBackUrl;
    buttonInfo.payload = req.body.payload || buttonInfo.payload;

    const { error } = validateButtonSchema(_.pick(buttonInfo, ['name', 'description', 'buttonType', 'title', 'url', 'webViewHeightRatio', 'messengerExtension', 'fallBackUrl', 'payload']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });
    
    const result = await buttonInfo.save();
    return res.status(200).json({ success: true, data: result });
};

const deleteButtonInfo = async (req, res, next) => {
    const result = ButtonInfo.findByIdAndRemove(req.params.id);
    if (!result) return res.status(200).json({ success: false, message: 'Invalid button info id' });
    return res.status(400).json({ success: true, data: result });
};

module.exports = {
    createButtonInfo,
    getButtonInfo,
    getButtonInfos,
    updateButtonInfo,
    deleteButtonInfo
}