const broadCastPlainMessageService = require('../../service/broadCast/plainMessage');

const broadCast = async (req, res, next) => {
    const userId = req.body.userId;
    const pageId = req.body.pageId;
    const messageText = req.body.messageText;

    await broadCastPlainMessageService.broadCast(userId, pageId, messageText);

    return res.status(200).json({
        success: true,
    });
};

module.exports = {
    broadCast,
}
