const { QuickReplyInfo, validateQuickReplyInfo } = require('../model/quickReply/quickReplyInfo');
const _ = require('lodash');

const createQuickReplyInfo = async (req, res, next) => {
    const { error } = validateQuickReplyInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const quickReplyInfo = new QuickReplyInfo(_.pick(req.body, ['content_type', 'title', 'payload']));
    const result = await quickReplyInfo.save();
    return res.status(200).json({ success: true, data: result });
};

const getQuickReplyInfo = async (req, res, next) => {
    const quickReplyInfo = await QuickReplyInfo.findById(req.params.id);
    if (!quickReplyInfo) return res.status(400).json({ success: false, message: 'Invalid quick replky info id' });
    return res.status(200).json({ success: true, data: quickReplyInfo });
};

const getQuickReplyInfos = async (req, res, next) => {
    const quickReplyInfos = await QuickReplyInfo.find();
    if (!quickReplyInfos) return res.status(400).json({ success: false, message: 'Internal server error' });
    return res.status(200).json({ success: true, data: quickReplyInfos });
};

const updateQuickReplyInfo = async (req, res, next) => {
    let quickReplyInfo = await QuickReplyInfo.findById(req.params.id);
    if (!quickReplyInfo) return res.status(400).json({ success: false, message: 'Invalid quick replky info id' });

    quickReplyInfo.content_type = req.body.content_type || quickReplyInfo.content_type;
    quickReplyInfo.title = req.body.title || quickReplyInfo.title;
    quickReplyInfo.payload = req.body.payload || quickReplyInfo.payload;

    const { error } = validateQuickReplyInfo(_.pick(quickReplyInfo, ['content_type', 'title', 'payload']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });
    
    const result = await quickReplyInfo.save();
    
    return res.status(200).json({ success: true, data: result });
};

const updateQuickReplyNextNodeTypeAndId = async (req, res, next) => {
    let quickReplyInfo = await QuickReplyInfo.findById(req.params.id);
    if (!quickReplyInfo) return res.status(400).json({ success: false, message: 'Invalid quick replky info id' });

    if (req.body.payload) {
        quickReplyInfo.payload = req.body.payload + quickReplyInfo.payload;
    }

    const { error } = validateQuickReplyInfo(_.pick(quickReplyInfo, ['content_type', 'title', 'payload']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });
    
    const result = await quickReplyInfo.save();
    
    return res.status(200).json({ success: true, data: result });
};

const deleteQuickReplyInfo = async (req, res, next) => {
    const quickReplyInfo = await QuickReplyInfo.findByIdAndRemove(req.params.id);
    if (!quickReplyInfo) return res.status(400).json({ success: false, message: 'Invalid quick reply info id' }); 
    return res.status(200).json({ success: true, data: quickReplyInfo });
};

module.exports = {
    createQuickReplyInfo,
    getQuickReplyInfo,
    getQuickReplyInfos,
    updateQuickReplyInfo,
    updateQuickReplyNextNodeTypeAndId,
    deleteQuickReplyInfo,
}