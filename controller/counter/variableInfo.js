const { VariableInfo, validateVariableInfo } = require('../../model/counter/variableInfo');
const _ = require('lodash');

const createVariableInfo = async (req, res, next) => {
    
    const { error } = validateVariableInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const variableInfo = new VariableInfo(_.pick(req.body, ['name', 'description', 'variableType', 'initialValue', 'value', 'botId', 'fbUserId']));
    const result = await variableInfo.save();

    return res.status(201).json({ success: true, data: result });
};

const getVariableInfo = async (req, res, next) => {
    const variableInfo = await VariableInfo.findById(req.params.id);
    if (!variableInfo) return res.status(400).json({ success: false, message: 'Invalid variable info id' });

    return res.status(200).json({ success: true, data: variableInfo });
};

const getVariableInfos = async (req, res, next) => {
    const variableInfos = await VariableInfo.find({});
    return res.status(200).json({ success: true, data: variableInfos });
};

const updateVariableInfo = async (req, res, next) => {
    let variableInfo = await VariableInfo.findById(req.params.id);
    if (!variableInfo) return res.status(400).json({ success: false, message: 'Invalid variableInfo Id' });

    variableInfo.name = req.body.name || variableInfo.name;
    variableInfo.description = req.body.description || variableInfo.description;
    variableInfo.variableType = req.body.variableType || variableInfo.variableType;
    variableInfo.initialValue = req.body.initialValue || variableInfo.initialValue;
    variableInfo.value = req.body.value || variableInfo.value;
    variableInfo.botId = req.body.botId || variableInfo.botId;
    variableInfo.fbUserId = req.body.fbUserId || variableInfo.fbUserId;

    const { error } = validateVariableInfo(_.pick(variableInfo, ['name', 'description', 'variableType', 'initialValue', 'value', 'botId', 'fbUserId']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await variableInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteVariableInfo = async (req, res, next) => {
    const variableInfo = await VariableInfo.findByIdAndRemove(req.params.id);
    if (!variableInfo) return res.status(400).json({ success: false, message: 'Invalid variable info id' });
    
    return res.status(200).json({ success: true, data: variableInfo });
};

module.exports = {
    createVariableInfo,
    getVariableInfo,
    getVariableInfos,
    updateVariableInfo,
    deleteVariableInfo,
};
