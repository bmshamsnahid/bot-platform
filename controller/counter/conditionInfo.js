const { ConditionInfo, validateCondition } = require('../../model/counter/conditionInfo');
const _ = require('lodash');

const createConditionInfo = async (req, res, next) => {

    const { error } = validateCondition(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const conditionInfo = new ConditionInfo(_.pick(req.body, ['name', 'description', 'variableInfo1', 'variableInfo2', 'condition', 'trueResultPayload', 'falseResultPayload', 'defaultPaylaod']));
    
    const result = await conditionInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const getConditionInfo = async (req, res, next) => {
    const conditionInfo = await ConditionInfo.findById(req.params.id);
    if (!conditionInfo) return res.status(400).json({ success: false, message: 'Invalid condition info id' });

    return res.status(200).json({ success: true, data: conditionInfo });
};

const getConditionInfos = async (req, res, next) => {
    const conditionInfos = await ConditionInfo.find({});
    return res.status(200).json({ success: true, data: conditionInfos });
};

const updateConditionInfo = async (req, res, next) => {
    let conditionInfo = await ConditionInfo.findById(req.params.id);
    if (!conditionInfo) return res.status(400).json({ success: true, message: 'Invalid condition info' });

    conditionInfo.name = req.body.name || conditionInfo.name;
    conditionInfo.description = req.body.description || conditionInfo.description;
    conditionInfo.variableInfo1 = req.body.variableInfo1 || conditionInfo.variableInfo1;
    conditionInfo.variableInfo2 = req.body.variableInfo2 || conditionInfo.variableInfo2;
    conditionInfo.condition = req.body.condition || conditionInfo.condition;
    conditionInfo.trueResultPayload = req.body.trueResultPayload || conditionInfo.trueResultPayload;
    conditionInfo.falseResultPayload = req.body.falseResultPayload || conditionInfo.falseResultPayload;
    conditionInfo.defaultPaylaod = req.body.defaultPaylaod || conditionInfo.defaultPaylaod;

    const { error } = validateCondition(_.pick(conditionInfo, ['name', 'description', 'variableInfo1', 'variableInfo2', 'condition', 'trueResultPayload', 'falseResultPayload', 'defaultPaylaod']));
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const result = await conditionInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteConditionInfo = async (req, res, next) => {
    const conditionInfo = await ConditionInfo.findByIdAndRemove(req.params.id);
    if(!conditionInfo) return res.status(400).json({ success: false, message: 'Invalid condition info id' });

    return res.status(200).json({ success: true, data: conditionInfo });
};

module.exports = {
    createConditionInfo,
    getConditionInfo,
    getConditionInfos,
    updateConditionInfo,
    deleteConditionInfo,
};
