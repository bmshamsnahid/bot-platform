const { OperationInfo, validateOperationInfo } = require('../../model/counter/operationInfo');
const _ = require('lodash');

const createOperationInfo = async (req, res, next) => {
    const { error } = validateOperationInfo(req.body);
    if (error) return res.status(400).json({ success: false, message: error.details[0].message });

    const operationInfo = new OperationInfo(_.pick(req.body, ['name', 'description', 'variableInfo1', 'variableInfo2', 'action', 'parentId']));

    const result = await operationInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const getOperationInfo = async (req, res, next) => {
    const operationInfo = await OperationInfo.findById(req.params.id);
    if (!operationInfo) return res.status(400).json({ success: false, message: 'Invalid operation info id' });

    return res.status(200).json({ success: true, data: operationInfo });
};

const getOperationInfos = async (req, res, next) => {
    const operationInfos = await OperationInfo.find({});
    return res.status(200).json({ status: 200, data: operationInfos });
};

const updateOperationInfo = async (req, res, next) => {
    let operationInfo = await OperationInfo.findById(req.params.id);
    if (!operationInfo) return res.status(400).json({ success: false, message: 'Invalid operation info id' });

    operationInfo.name = req.body.name || operationInfo.name;
    operationInfo.description = req.body.description || operationInfo.description;
    operationInfo.variableInfo1 = req.body.variableInfo1 || operationInfo.variableInfo1;
    operationInfo.variableInfo2 = req.body.variableInfo2 || operationInfo.variableInfo2;
    operationInfo.action = req.body.action || operationInfo.action;
    operationInfo.parentId = req.body.parentId || operationInfo.parentId;

    const { error } = validateOperationInfo(_.pick(operationInfo, ['name', 'description', 'variableInfo1', 'variableInfo2', 'action', 'parentId']));
    if (error) return res.status(400).json({ success: false, data: error.details[0].message });

    const result = await operationInfo.save();

    return res.status(200).json({ success: true, data: result });
};

const deleteOperationInfo = async (req, res, next) => {
    const operationInfo = await OperationInfo.findByIdAndRemove(req.params.id);
    if (!operationInfo) return res.status(400).json({ success: false, message: 'Invalid operation info id' });
};

module.exports = {
    createOperationInfo,
    getOperationInfo,
    getOperationInfos,
    updateOperationInfo,
    deleteOperationInfo
}
