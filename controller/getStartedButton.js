const request = require('../helper/requestPromise');
const baseUrl = require('../baseUrl');
const { BotInfo } = require('../model/bot/botInfo');

const createGetStartedButton = async (req, res, next) => {
    const botInfo = await BotInfo.findOne({ pageId: req.body.pageId });
    if (!botInfo) return res.status(400).json({ success: false, message: 'Invalid page Id' });
    
    const url = baseUrl.getStartedButton + '?access_token=' + botInfo.pageAccessToken;

    const  messageData = {
        "get_started": {
            "payload": req.body.payload
        }
    };

    const options = {
        uri: url,
        qs: { access_token: botInfo.pageAccessToken },
        method: 'post',
        json: messageData
    };
    
    const response = await request(options);

    res.status(200).json({ success: true, data: response });
};

const deleteGetStartedButton = async (req, res, next) => {
    const botInfo = await BotInfo.findOne({ pageId: req.query.pageId });
    if (!botInfo) return res.status(400).json({ success: false, message: 'Invalid page Id' });

    const url = baseUrl.getStartedButton + '?access_token=' + botInfo.pageAccessToken;
    
    const messageData = {
        "fields": [
            "get_started"
        ]
    };

    const options = {
        uri: url,
        qs: { access_token: botInfo.pageAccessToken },
        method: 'delete',
        json: messageData
    };
    
    const response = await request(options);

    res.status(200).json({ success: true, data: response });
};

module.exports = {
    createGetStartedButton,
    deleteGetStartedButton
};
