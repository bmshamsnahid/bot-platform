const sendTextMessageLib = require('../messageSend/text');
const sendQuickReplyMessageLib = require('../messageSend/quickReply');
const sendPlainMessageLib = require('../messageSend/plainMessage');

const flowHandle = require('../messageSend/flowHandle');

const validateEmail = require('validate-email-node-js');
const validatePhoneNumber = require('validate-phone-number-node-js');

const { TextMessage } = require('../model/message/text');

const handleQuickReply = async (quickReply, senderId, pageId, event) => {
    const payload = quickReply.payload;
    await sendMessageUsingPayload(payload, senderId, pageId);
};

const handlePlainMessage = async (userLastNode, senderId, pageId, event) => {
    const lastPlainMessage = await TextMessage.findById(userLastNode.lastNodeId);
    const payload = lastPlainMessage.payload;

    if (payload) {
        const payloadArr = payload.split('-');
        const nextNodeType = payloadArr[0];
        const nextNodeId = payloadArr[1];
        if (nextNodeType == 'plain') {
            return sendPlainMessageLib(senderId, nextNodeId, pageId);
        } else if (nextNodeType == 'quick reply') {
            sendQuickReplyMessageLib(pageId, senderId, nextNodeId);
        }
    }
};

const handleGetStartedPayload = async (payload, senderId, pageId) => {
    await sendMessageUsingPayload(payload, senderId, pageId);
}

module.exports = {
    handleQuickReply,
    handlePlainMessage,
    handleGetStartedPayload,
};

const sendMessageUsingPayload = async (payload, senderId, pageId) => {
    const quickReplyPayload = payload.split('-');
    
    // payload is consist of nextMessageType-nextMessageId-previousMessageType-previousMessageText
    const nextMessageType = quickReplyPayload[0];
    const nextMessageId = quickReplyPayload[1];

    if (nextMessageType === 'quick reply') {
        sendQuickReplyMessageLib(pageId, senderId, nextMessageId);
    } else if (nextMessageType === 'flow') {
        flowHandle(nextMessageId, senderId, pageId);
    } else if (nextMessageType == 'plain') {
        sendPlainMessageLib(senderId, nextMessageId, pageId);
    } else {
        const emailAddress = validateEmail.validate(payload);
        const phoneNumber = validatePhoneNumber.validate(payload) && payload.includes('+');
        if (emailAddress) {
            sendTextMessageLib(senderId, 'Found a email address: ' + payload, pageId);
        } else if (phoneNumber) {
            sendTextMessageLib(senderId, 'Found a phone Number: ' + payload, pageId);
        } else {
            sendTextMessageLib(senderId, 'Unhandled payload: ' + payload, pageId);
        }
    }
};
