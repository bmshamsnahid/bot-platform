const { UserConversion } = require('../../model/UserInteraction/userConversion');
const { MessageSession } = require('../../model/UserInteraction/messageSession');

//retriving all the conversions
const getAllUserConversions = async (req, res, next) => {
    const result = await UserConversion.find();
    return res.status(200).json({ success: true, data: result });
};

//get a user conversion to any page
const getUserConversion = async (req, res, next) => {
    
};

const getPageSpecificUserConversion = async (req, res, next) => {

};

const getUserSpecificUserConversion = async (req, res, next) => {

};

const getPageAndUserSpecificUserConversion = async (req, res, next) => {

};

const getSessions = async (req, res, next) => {

};

const getSessionConversions = async (req, res, next) => {

};

const getSessionConversion = async () => {

};

module.exports = {

};