const { FlowInfo } = require('../model/flow/flowInfo');
const flowHandle = require('./flowHandle');
const lastNodeChecker = require('../helper/lastNodeChecker');

/*
 * Send a text message using the Send API.
 *
 */
module.exports = async (recipientId, messageText, pageId, event = null) => {
    const flowInfo = await FlowInfo.findOne({
        triggerKeyword: { 
            $in: messageText
         }, 
         pagesId: {
             $in: pageId
         }
    });

    if (flowInfo) {
        flowHandle(flowInfo._id, recipientId, pageId);
    } else {
        // check the last node
        await lastNodeChecker(pageId, recipientId, event, messageText);
    }
};
