const request = require('../helper/requestPromise');
const { BotInfo } = require('../model/bot/botInfo');
const userInteractionHelper = require('../helper/saveUserInteraction');
const userLastNodeService = require('../service/userConversion/userLastNode');

const chalk = require('chalk');

/*
 * Call the Send API. The message data goes in the body. If successful, we'll
 * get the message id in a response
 *
 */
module.exports = async (messageData, recipientID, senderID, messageType, messageId = null,
    uri = 'https://graph.facebook.com/v2.6/me/messages', method= 'POST') => {

    await userInteractionHelper(senderID, recipientID, null, messageData, 'SEND_FROM_SERVER', messageType, messageId);

    const botInfo = await BotInfo.findOne({ pageId: recipientID });
    
    // if the botInfo page id and the recipient id matched, then this is the last message for the sender id 
    // confirm this is sent from the server to the page
    if (messageId && (recipientID == botInfo.pageId)) {
        // message type is a quick reply message 
        if (messageType == 'quick reply' || messageType == 'plain') {
            await userLastNodeService.handleUserLastNode(recipientID, senderID, messageId, messageType);
        }
    }
    const options = {
        uri: uri,
        qs: { access_token: botInfo.pageAccessToken },
        method: method,
        json: messageData
    };

    const res = await request(options);
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('Message sending operation completed successfully.'));
    console.log (res);
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('*********************************************************'));
    console.log(chalk.green('*********************************************************'));
};
