const callSendApi = require('./callSendApi');
const { QuickReplyMessage } = require('../model/message/quickReplyMessage');
const { QuickReplyInfo } = require('../model/quickReply/quickReplyInfo');
/*
 * Send a message with Quick Reply buttons.
 *
 */
module.exports = async (pageId, senderId, quickReplyMessageId) => {

    const quickReplyMessage = await QuickReplyMessage.findById(quickReplyMessageId);
    const quickReplies = await getQuickReplies(quickReplyMessage.quick_replies);

    const messageData = {
        recipient: { id: senderId },
        message: {
            text: quickReplyMessage.text,
            quick_replies: quickReplies,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };
    callSendApi(messageData, pageId, senderId, 'quick reply', quickReplyMessageId);
};

const getQuickReplies = async (quickRepliesId) => {
    const quickReplies = [];
    for (let index=0; index<quickRepliesId.length; index++) {
        const quickReplyInfo = await QuickReplyInfo.findById(quickRepliesId[index]).select('content_type title payload -_id');
        quickReplies.push(quickReplyInfo);
    }
    return quickReplies;
};