const callSendApi = require('./callSendApi');

/*
 * Send a text message using the Send API.
 *
 */
module.exports = async (recipientId, messageText, pageId) => {
    const messageData = {
        recipient: { id: recipientId },
        message: {
            text: messageText,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };
    callSendApi(messageData, pageId, recipientId, 'plain');
};
