const callSendApi = require('./callSendApi');

/*
 * Turn typing indicator on
 *
 */
module.exports = (recipientId) => {
    console.log("Turning typing indicator on");

    const messageData = {
        recipient: {
            id: recipientId
        },
        sender_action: "typing_on"
    };

    callSendApi(messageData);
};