const elementInfoService = require('../service/element/elementInfo');
const textMessageSend = require('./text');
const callSendApi = require('./callSendApi');
const dataType = require('../dataType');

module.exports = async (pageId, senderId, templateInfo) => {
    const { error, data } = await elementInfoService.getMultipleElementInfos(templateInfo.elements);
    if (error) return textMessageSend(senderId, 'Internal server error: ' + error.message, pageId);
    
    const elementInfos = await elementInfoService.modifyElementForTemplate(data);

    const messageData = {
        "recipient": {
            id: senderId
        },
        "message":{
            "attachment":{
                "type": 'template',
                "payload":{
                    "template_type": templateInfo.templateType,
                    "elements":  elementInfos
                }
            }
        }
    };
    
    callSendApi(messageData, pageId, senderId, dataType.genericTemplateType, templateInfo._id);
};

