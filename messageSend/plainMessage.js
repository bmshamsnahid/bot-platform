const callSendApi = require('./callSendApi');
const { TextMessage } = require('../model/message/text');
const userLogService = require('../service/userConversion/userLog');
const chalk = require('chalk');

/*
 * Send a text message using the Send API.
 *
 */
module.exports = async (recipientId, plainMessageId, pageId) => {
    const plainMessage = await TextMessage.findById(plainMessageId);
    const messageText = plainMessage.text;

    if (plainMessage.isLastMessage) {
        await userLogService.createUserLog(recipientId, pageId);
    }

    const messageData = {
        recipient: { id: recipientId },
        message: {
            text: messageText,
            metadata: "DEVELOPER_DEFINED_METADATA"
        }
    };
    callSendApi(messageData, pageId, recipientId, 'plain', plainMessageId);
};
