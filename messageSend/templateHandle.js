const { TemplateInfo } = require('../model/template/templateInfo');
const textMessageSend = require('./text');
const genericTemplateHandle = require('./genericTemplate');

module.exports = async (pageId, senderId, templateId) => {
    const templateInfo = await TemplateInfo.findById(templateId);
    if (templateInfo.templateType === 'generic') {
        genericTemplateHandle(pageId, senderId, templateInfo);
    } else {
        textMessageSend(senderId, 'Invalid template', pageId);
    }
};