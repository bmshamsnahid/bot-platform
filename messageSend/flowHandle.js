const variableInfoService = require('../service/counter/variableInfo');
const { FlowInfo } = require('../model/flow/flowInfo');
const quickReplyMessageSend = require('./quickReply');
const textMessageSend = require('./text');
const plainMessageSend = require('./plainMessage');
const templateHandle = require('./templateHandle');
const chalk = require('chalk');

module.exports = async (flowId, senderId, pageId) => {
    const flowInfo = await FlowInfo.findById(flowId);
    if (flowInfo.initialFlow) {
        console.log(chalk.red('Got a starting flow'));
        initiateAllBotsVariable(flowInfo.botsId, senderId);
    }
    const payload = flowInfo.payload;
    if (payload) {
        const nextMessageInfo = payload.split('-');

        const nextMessageType = nextMessageInfo[0];
        const nextMessageId = nextMessageInfo[1];

        if (nextMessageType === 'quick reply') {
            quickReplyMessageSend(pageId, senderId, nextMessageId);
        } else if (nextMessageType == 'plain') {
            plainMessageSend(senderId, nextMessageId, pageId);
        } else if (nextMessageType === 'template') {
            templateHandle(pageId, senderId, nextMessageId);
        } else {
            textMessageSend(senderId, 'Empty Flow Triggered', pageId);
        }
    } else {
        textMessageSend(senderId, 'No Message Exist', pageId);
    }
};

const initiateAllBotsVariable = async (botsId, fbUserId) => {
    for (let index=0; index<botsId.length; index++) {
        await variableInfoService.initiateBotVariableInfos(botsId[index], fbUserId);
    }
    return;
};
