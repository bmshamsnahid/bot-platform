module.exports = (req, res, next) => {
    if (!req.user.isAdmin) {
        return res.status(403).json({ success: false, message: 'Admin credential required.' });
    }
    return next();
};
